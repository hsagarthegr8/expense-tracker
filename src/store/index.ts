import { createStore, combineReducers, applyMiddleware  } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import authReducer from './auth/reducer'
import walletReducer from './wallet/reducer'
import transactionReducer from './transactions/reducer'



const reducers = combineReducers({
    auth: authReducer, 
    wallets: walletReducer,
    transactions: transactionReducer
})


const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)))

export default store

export * from './types'


