import { AuthState } from './auth/types'
import { WalletState } from './wallet/types'
import { TransactionState } from './transactions/types'

export interface ApplicationState {
    auth: AuthState,
    wallets: WalletState,
    transactions: TransactionState
}




