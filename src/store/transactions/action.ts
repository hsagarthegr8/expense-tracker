import {
    FETCH_TRANSACTIONS, 
    FetchTransactionsAction, 
    TransactionState, 
    Transaction, 
    AddTransactionAction, 
    ADD_TRANSACTION, 
    DELETE_ALL_TRANSACTIONS_OF_A_WALLET,
    DeleteAllTransactionsOfAWalletAction,
    DeleteTransactionAction,
    DELETE_TRANSACTION,
    UpdateTransactionAction,
    UPDATE_TRANSACTION
} from './types'

import { Dispatch } from 'redux'
import { Api } from '../../api'
import { AxiosResponse, AxiosError } from 'axios'
import { Wallet } from '../wallet/types';

export const fetchTransactionList = (transactions: TransactionState): FetchTransactionsAction => {
    return {
        type: FETCH_TRANSACTIONS,
        transactions: transactions
    }
}

export const fetchTransactions = () => {
    return (dispatch: Dispatch) => {
        const api = Api()
        api.get('transactions/')
        .then((res: AxiosResponse) => {
            const transactions = res.data
            dispatch(fetchTransactionList(transactions))
        })
        .catch((err: AxiosError) => {
            console.log(err.message)
        })
    }
}

export const addTransaction = (transaction: Transaction): AddTransactionAction => {
    return {
        type: ADD_TRANSACTION,
        transaction: transaction
    }
}

export const updateTransaction = (transaction: Transaction): UpdateTransactionAction => {
    return {
        type: UPDATE_TRANSACTION,
        transaction: transaction
    }
}

export const deleteTransaction = (transaction: Transaction): DeleteTransactionAction => {
    return {
        type: DELETE_TRANSACTION,
        id: transaction.id
    }
}

export const deleteAllTransactionsOfAWallet = (wallet: Wallet): DeleteAllTransactionsOfAWalletAction => {
    return {
        type: DELETE_ALL_TRANSACTIONS_OF_A_WALLET,
        wallet: wallet
    }
}
