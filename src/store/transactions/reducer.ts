import {FETCH_TRANSACTIONS, FetchTransactionsAction, TransactionState, TransactionActionTypes, ADD_TRANSACTION, DELETE_ALL_TRANSACTIONS_OF_A_WALLET, Transaction, DELETE_TRANSACTION, UPDATE_TRANSACTION} from './types'


const initialState: TransactionState = []

const transactionReducer = (state = initialState, action: TransactionActionTypes ) => {
    switch (action.type) {
        case ADD_TRANSACTION:
            return [action.transaction, ...state]

        case UPDATE_TRANSACTION:
            return state.map((transaction: Transaction) => {
                if (transaction.id === action.transaction.id) 
                    return action.transaction
                return transaction
            })

        case DELETE_TRANSACTION:
            return state.filter((transaction: Transaction) => transaction.id !== action.id)

        case DELETE_ALL_TRANSACTIONS_OF_A_WALLET:
            return state.filter((transaction: Transaction) => transaction.wallet != action.wallet.id)

        case FETCH_TRANSACTIONS:
            return action.transactions
            
        default:
            return state
    }
}

export default transactionReducer