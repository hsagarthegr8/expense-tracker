import { Wallet } from "../wallet/types";

export interface Transaction {
    id: number,
    type: string,
    amount: number,
    description: string,
    date: Date,
    is_transfer: boolean,
    timestamp: Date,
    updated: Date,
    wallet: number
}

export interface TransactionState extends Array<Transaction> {}

export const FETCH_TRANSACTIONS = 'FETCH_TRANSACTIONS'
export const ADD_TRANSACTION = 'ADD_TRANSACTION'
export const UPDATE_TRANSACTION = 'UPDATE_TRANSACTION'
export const DELETE_TRANSACTION = 'DELETE_TRANSACTION'
export const DELETE_ALL_TRANSACTIONS_OF_A_WALLET = 'DELETE_ALL_TRANSACTIONS_OF_A_WALLET'


export interface FetchTransactionsAction {
    type: typeof FETCH_TRANSACTIONS,
    transactions: Transaction[]
}

export interface AddTransactionAction {
    type: typeof ADD_TRANSACTION,
    transaction: Transaction
}

export interface UpdateTransactionAction {
    type: typeof UPDATE_TRANSACTION,
    transaction: Transaction
}

export interface DeleteTransactionAction {
    type: typeof DELETE_TRANSACTION,
    id: number
}

export interface DeleteAllTransactionsOfAWalletAction {
    type: typeof DELETE_ALL_TRANSACTIONS_OF_A_WALLET
    wallet: Wallet
}

export type TransactionActionTypes = FetchTransactionsAction 
    | AddTransactionAction | UpdateTransactionAction | DeleteTransactionAction 
    | DeleteAllTransactionsOfAWalletAction