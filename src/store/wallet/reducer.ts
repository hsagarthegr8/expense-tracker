
import { Wallet, WalletActionTypes, WalletState, ADD_WALLET, DELETE_WALLET, TRANSFER_BALANCE, FETCH_WALLETS, UPDATE_WALLET } from './types'


const initialState: WalletState = []

const walletReducer = (state = initialState, action: WalletActionTypes): WalletState => {
    switch (action.type) {
        case FETCH_WALLETS:
            return action.wallets

        case ADD_WALLET:
            return [...state, action.wallet]

        case DELETE_WALLET:
            return state.filter((wallet: Wallet) => wallet.id != action.id)
        
        case UPDATE_WALLET:
            return state.map((wallet:Wallet) => {
                if (wallet.id == action.wallet.id) {
                    return action.wallet
                }
                return wallet
            })

        case TRANSFER_BALANCE:
            return state.map((wallet: Wallet) => {
                if (wallet.id === action.fromId)
                    return {
                        ...wallet,
                        balance: wallet.balance - action.amount
                    }
                else if (wallet.id === action.toId) {
                    return {
                        ...wallet,
                        balance: wallet.balance + action.amount
                    }
                }
                return wallet
            })

        default:
            return state
    }
}

export default walletReducer