import { 
    Wallet, 
    ADD_WALLET, 
    DELETE_WALLET, 
    TRANSFER_BALANCE, 
    FETCH_WALLETS, 
    AddWalletAction, 
    DeleteWalletAction,
    TransferBalanceAction, 
    FetchWalletsAction, 
    UpdateWalletAction,
    UPDATE_WALLET
} from './types'

import { Dispatch } from 'redux'
import { Api } from '../../api'
import { AxiosResponse, AxiosError } from 'axios'


export const fetchWalletsList = (wallets: Wallet[]):FetchWalletsAction => {
    return {
        type: FETCH_WALLETS,
        wallets: wallets
    }
}
export const fetchWallets = () => {
    return (dispatch:Dispatch) => {
        const api = Api()
        api.get('/wallets/')
        .then((res: AxiosResponse) => {
            const wallets = res.data
            dispatch(fetchWalletsList(wallets))
        })
        .catch((err: AxiosError) => {
            console.log(err.message)
        })
    }
}

export const addWallet = (wallet: Wallet) :AddWalletAction => {
    return {
        type: ADD_WALLET,
        wallet: wallet
    }
}

export const deleteWallet = (wallet: Wallet): DeleteWalletAction => {
    return {
        type: DELETE_WALLET,
        id: wallet.id
    }
}

export const updateWallet = (wallet: Wallet): UpdateWalletAction => {
    return {
        type: UPDATE_WALLET,
        wallet: wallet
    }
}


export const transferBalanceBetweenWallet = (from: number, to: number, amount:number): TransferBalanceAction => {
    return {
        type: TRANSFER_BALANCE,
        fromId: from,
        toId: to,
        amount: amount
    }
}
