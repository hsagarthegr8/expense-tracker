export interface Wallet {
    id: number,
    name: string,
    balance: number,
    category: string
}

export interface WalletState extends Array<Wallet>{}


export const ADD_WALLET = 'ADD_WALLET'
export const DELETE_WALLET = 'DELETE_WALLET'
export const TRANSFER_BALANCE = 'TRANSFER_BALANCE'
export const FETCH_WALLETS = 'FETCH_WALLETS'
export const UPDATE_WALLET = 'UPDATE_WALLET'

export interface AddWalletAction {
    type: typeof ADD_WALLET,
    wallet: Wallet
}

export interface DeleteWalletAction {
    type: typeof DELETE_WALLET,
    id: number
}

export interface UpdateWalletAction {
    type: typeof UPDATE_WALLET
    wallet: Wallet
}
export interface TransferBalanceAction {
    type: typeof TRANSFER_BALANCE,
    fromId: number,
    toId: number,
    amount: number
}

export interface FetchWalletsAction {
    type: typeof FETCH_WALLETS,
    wallets: Wallet[]
}

export type WalletActionTypes = AddWalletAction | DeleteWalletAction | TransferBalanceAction | FetchWalletsAction | UpdateWalletAction