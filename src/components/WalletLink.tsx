import React, { FC, Fragment } from 'react'
import { connect } from 'react-redux'
import { ApplicationState } from '../store'
import { Wallet } from '../store/wallet/types'
import { Typography, Link, Tooltip, Theme, createStyles, WithStyles, withStyles } from '@material-ui/core'
import { getCategory } from '../utils'

interface OwnProps {
    id: number
}

interface State {
    wallet?: Wallet
}

const styles = (theme: Theme) => createStyles({
    tooltip: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 500,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
        '& b': {
            fontWeight: theme.typography.fontWeightMedium,
        },
    },
})

const WalletLink: FC<OwnProps & State & WithStyles<typeof styles>> = (props) => {
    const { wallet, classes } = props
    return (
        wallet ?
            <Tooltip placement="right-end" interactive
                classes={{
                    tooltip: classes.tooltip,
                }}
                title={
                    <Fragment>
                        <Typography>Wallet name: <Typography inline>{wallet.name}</Typography></Typography>
                        <Typography>Wallet Category: <Typography inline>{getCategory(wallet)}</Typography></Typography>
                        <Typography>Available Balance: <Typography inline>{wallet.balance.toLocaleString('en-IN')}</Typography></Typography>
                    </Fragment>
                }
            >
                <Link>
                    {wallet.name}
                </Link>
            </Tooltip>
            :
            null
    )
}

const mapStateToProps = (state: ApplicationState, ownProps: OwnProps): State => {
    const { id } = ownProps
    return {
        wallet: state.wallets.find((wallet: Wallet) => wallet.id === id)
    }
}

export default connect(mapStateToProps)(withStyles(styles)(WalletLink))