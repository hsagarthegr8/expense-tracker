import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Paper,
    Typography,
    WithStyles,
    createStyles,
    Theme,
    withStyles,
    TableHead,
    Table,
    TableRow,
    TableCell,
    TableBody,
    Fab,
    Tooltip,
    Grid
} from '@material-ui/core'
import { Scrollbar } from 'react-scrollbars-custom'

import AddIcon from '@material-ui/icons/Add'

import { Wallet } from '../store/wallet/types'
import { ApplicationState } from '../store'
import { fetchWallets } from '../store/wallet/action'
import WalletListItem from './WalletListItem'
import { AddWalletDialog, EditWalletDialog, TransferBalanceDialog } from './dialogs'
import DeleteWalletDialog from './dialogs/DeleteWalletDialog';
import NoWalletMessage from './NoWalletMessage';


type Props = {
    wallets: Wallet[],
}

type Action = {
    fetchWallets: Function
}

interface DialogState { 
    add: boolean
    edit: boolean
    delete: boolean,
    transferBalance: boolean
}

interface State {
    dialog: DialogState
    selectedWallet?: Wallet
}

const styles = (theme: Theme) => createStyles({
    paper: {
        padding: theme.spacing.unit * 3,
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    box: {
        marginBottom: 8,
        height: '20em'
    },
    alignRight: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    actionButtom: {
        textTransform: 'uppercase',
        margin: theme.spacing.unit,
        width: 152
    },
})

class WalletDashboard extends Component<Props & Action & WithStyles<typeof styles>, State> {
    state = {
        dialog: {
            add: false,
            edit: false,
            transferBalance: false,
            delete: false
        },
        selectedWallet: undefined
    }

    componentDidMount() {
        document.title += ' | Dashboard'
        const { fetchWallets } = this.props
        fetchWallets()
    }

    handleDialogOpen = (dialog: string) => {
        this.setState((prevState: State) => ({
            dialog: {
                ...prevState.dialog,
                [dialog]: true
            }
        }))
    }

    handleDialogClose = (dialog: string) => {
        this.setState((prevState: State) => ({
            dialog: {
                ...prevState.dialog,
                [dialog]: false
            }
        }))
    }

    handleWalletSelect = (wallet: Wallet, dialog: string) => {
        this.setState({
            selectedWallet: wallet
        })
        this.handleDialogOpen(dialog)
    }

    
    render() {
        const { classes, wallets } = this.props
        const { dialog, selectedWallet } = this.state
        return (
            <Paper className={classes.paper}>
                
                <div className={classes.box}>
                    <Typography variant="h6">
                        Your Wallets
                        <Tooltip title="Add Wallet">
                            <Fab 
                                size="small"
                                style={{float: "right"}} 
                                color="primary" 
                                onClick={()=>this.handleDialogOpen('add')}
                            >
                                <AddIcon />
                            </Fab>
                        </Tooltip>
                        <AddWalletDialog open={dialog.add} handleClose={()=>this.handleDialogClose('add')} />
                    </Typography>
                    
                    {wallets.length ?
                        <Scrollbar style={{maxWidth:'100%', height:'17em'}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell style={{minWidth: '16em'}}>Name</TableCell>
                                    <TableCell style={{minWidth: '12em'}}>Balance</TableCell>
                                    <TableCell style={{minWidth: '15em'}}>Category</TableCell>
                                    <TableCell style={{minWidth: '15em'}}>Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {wallets.map((wallet: Wallet) => 
                                    <WalletListItem 
                                        key={wallet.id} 
                                        wallet={wallet} 
                                        handleClick={this.handleWalletSelect} 
                                    />
                                )}
                            </TableBody>
                        </Table>
                        </Scrollbar>
                        :
                        <NoWalletMessage />
                    }

                    { dialog.edit && selectedWallet ?
                        <EditWalletDialog 
                            wallet={selectedWallet} 
                            open={dialog.edit}
                            handleClose={()=>this.handleDialogClose('edit')} 
                        />
                    :
                    null
                    }

                    { dialog.transferBalance && selectedWallet ?
                        <TransferBalanceDialog
                            wallet={selectedWallet} 
                            open={dialog.transferBalance}
                            handleClose={()=>this.handleDialogClose('transferBalance')} 
                        />
                    :
                    null
                    }

                    { dialog.delete && selectedWallet ?
                        <DeleteWalletDialog 
                            wallet={selectedWallet}
                            open={dialog.delete}
                            handleClose={()=>this.handleDialogClose('delete')}
                        />
                        :
                        null
                    }

                </div>
            </Paper>
        )
    }
}


const mapStateToProps = (state: ApplicationState): Props => {
    return {
        wallets: state.wallets
    }
}

const mapDispatchToProps = {
    fetchWallets,
}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(WalletDashboard))