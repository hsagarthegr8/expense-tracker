import React, { Component, Fragment } from 'react'
import { TableRow, TableCell, IconButton, Tooltip } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import TransferIcon from '@material-ui/icons/ImportExport'

import { EditWalletDialog } from './dialogs'
import { Wallet } from '../store/wallet/types'
import { getCategory } from '../utils'




interface OwnProps {
    wallet: Wallet,
    handleClick: Function
}

interface State {
    editDialogOpen: boolean,

}

class WalletListItem extends Component<OwnProps> {

    render() {
        const { wallet, handleClick } = this.props
        return (
            <Fragment>
                <TableRow>
                    <TableCell style={{minWidth: '16em'}}>{wallet.name}</TableCell>
                    <TableCell style={{minWidth: '12em'}}>₹ {wallet.balance.toLocaleString('en-IN')}</TableCell>
                    <TableCell style={{minWidth: '15em'}}>{getCategory(wallet)}</TableCell>
                    <TableCell style={{minWidth: '15em'}}>
                        <Tooltip title="Edit Wallet">
                            <IconButton onClick={() => handleClick(wallet, 'edit')}>
                                <EditIcon fontSize="small" />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Transfer Balance">
                            <IconButton onClick={() => handleClick(wallet, 'transferBalance')}>
                                <TransferIcon fontSize="small" />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Delete Wallet" onClick={() => handleClick(wallet, 'delete')}>
                            <IconButton>
                                <DeleteIcon fontSize="small" color="error"/>
                            </IconButton>
                        </Tooltip>
                    </TableCell>
                </TableRow>
            </Fragment>
        )
    }
}

export default WalletListItem