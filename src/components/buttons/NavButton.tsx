import React, { ComponentType } from 'react'

import { Button, Tooltip, Typography } from '@material-ui/core'
import { SvgIconProps } from '@material-ui/core/SvgIcon'

interface Props {
    text: string,
    Icon: ComponentType<SvgIconProps>,
    onClick: Function
} 

const NavButton = (props:Props) => {
    const { text, Icon, onClick } = props
    return (
        <Tooltip title={text}>
            <Button color="primary" onClick={()=>onClick()}>
                <Icon />
                &nbsp;
                <Typography inline color="primary" variant="button">{text}</Typography>
            </Button>
        </Tooltip>
    )
}

export default NavButton