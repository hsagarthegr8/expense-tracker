import React from 'react'
import { Grid, Typography } from '@material-ui/core'
import WarningIcon from '@material-ui/icons/Warning'

const NoWalletMessage = () => {
    return (
        <Grid container alignItems="center" style={{height:'15em', width:'100%'}}>
            <Typography align="center" variant="subtitle1" style={{margin: 'auto', color:"#D3D3D3"}}>
                <Typography align="center" style={{color: "#D3D3D3"}}>
                    <WarningIcon fontSize="large" />
                </Typography>
                 You have not added a Wallet Yet. Please Add a Wallet.
            </Typography>
        </Grid> 
    )
}

export default NoWalletMessage