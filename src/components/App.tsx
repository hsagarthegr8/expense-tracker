import React, { Component, Fragment } from 'react'
import { CssBaseline, createMuiTheme, MuiThemeProvider, Theme } from '@material-ui/core'
import { blue, indigo } from '@material-ui/core/colors'

import { TopBar } from './TopBar'
import Dashboard from './Dashboard'
import { logIn } from '../store/auth/action';
import { connect } from 'react-redux';
import { Api, verifyTokenUrl, loginUrl } from '../api';
import { AxiosResponse, AxiosError } from 'axios';

const theme: Theme = createMuiTheme({
	palette: {
		secondary: {
			main: blue[900]
		},
		primary: {
			main: indigo[700]
		}
	},
	typography: {
		// Use the system font instead of the default Roboto font.
		useNextVariants: true,
		fontFamily: [
			'"Lato"',
			'sans-serif'
		].join(',')
	}
})


interface Action {
	logIn: Function
}

class App extends Component<Action> {
	componentDidMount() {
		const { logIn } = this.props
		const token = localStorage.getItem('token')
		if (token) {
			const api = Api()
			api.post(verifyTokenUrl, {token: token})
			.then((res: AxiosResponse) => {
				logIn(token)
			})
			.catch((err: AxiosError) => {
				
			})
		}
	}

	render() {
		return (
			<Fragment>
				<CssBaseline />
				<MuiThemeProvider theme={theme}>
					<TopBar />
					<div style={{marginTop: '2em'}}>
						<Dashboard />
					</div>
					
				</MuiThemeProvider>
			</Fragment>
		)
	}
}


const mapDispatchToProps: Action = {
	logIn
}

export default connect(null, mapDispatchToProps)(App)
