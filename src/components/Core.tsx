import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment'

import store from '../store'

import App from './App'


export default () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <MuiPickersUtilsProvider utils={MomentUtils} >
                    <App />
                </MuiPickersUtilsProvider>
            </BrowserRouter>
        </Provider>
    )
}

