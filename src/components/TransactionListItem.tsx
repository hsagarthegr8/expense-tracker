import React, { FC, Fragment } from 'react'
import { Transaction } from '../store/transactions/types'
import {
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
    Typography,
    Theme,
    createStyles,
    withStyles,
    WithStyles,
    IconButton,
    Grid,
    Tooltip
} from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'

import WalletLink from './WalletLink'

const styles = (theme: Theme) => createStyles({
    credited: {
        color: "green"
    },
    debited: {
        color: "red"
    },

    actionButton: {
        float: 'right'
    }
})

interface OwnProps {
    transaction: Transaction
    handleSelect: Function
}

const TransactionListItem: FC<OwnProps & WithStyles<typeof styles>> = (props) => {
    const { transaction, handleSelect, classes } = props
    return (
        <Grid container>
            <Grid item xs={12} sm={10}>
                <ListItem>
                    <ListItemText secondary={
                        <Fragment>
                            <Typography variant="caption" inline >
                                {transaction.description}&nbsp;
                            </Typography>
                            <div style={{ width: '10em', display: 'inline-block' }}>
                                <Typography inline variant="caption" color="textSecondary">
                                    | {transaction.date}
                                </Typography>
                            </div>

                        </Fragment>
                    }>
                        {transaction.type === 'I' ?
                            <Fragment>
                                <Typography className={classes.credited} inline>Credited </Typography>
                                <Typography inline>
                                    to <WalletLink id={transaction.wallet} />
                                </Typography>
                            </Fragment>
                            :
                            <Fragment>
                                <Typography className={classes.debited} inline>Debited </Typography>
                                <Typography inline>
                                    from <WalletLink id={transaction.wallet} />
                                </Typography>
                            </Fragment>

                        }

                    </ListItemText>
                    <ListItemSecondaryAction>
                        {transaction.type === 'I' ?
                            <Typography className={classes.credited}>+ ₹ {transaction.amount.toLocaleString('en-IN')}</Typography>
                            :
                            <Typography className={classes.debited}>- ₹ {transaction.amount.toLocaleString('en-IN')}</Typography>
                        }

                    </ListItemSecondaryAction>
                </ListItem>
            </Grid>
            <Grid item xs={12} sm={2}>
                <Tooltip title="Delete Transaction">
                    <IconButton
                        disabled={transaction.is_transfer}
                        className={classes.actionButton}
                        onClick={() => handleSelect(transaction, 'delete')}
                    >
                        <DeleteIcon fontSize="small" color={transaction.is_transfer ? "disabled" : "error"} />
                    </IconButton>
                </Tooltip>
                <Tooltip title="Edit Transaction">
                    <IconButton
                        disabled={transaction.is_transfer}
                        className={classes.actionButton}
                        onClick={() => handleSelect(transaction, 'edit')}
                    >
                        <EditIcon fontSize="small" color={transaction.is_transfer ? "disabled" : "action"} />
                    </IconButton>
                </Tooltip>
            </Grid>
        </Grid>

    )
}

export default withStyles(styles)(TransactionListItem)