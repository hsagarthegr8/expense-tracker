import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import {
    AppBar,
    Toolbar,
    Grid,
    Typography,
    withStyles,
    WithStyles,
} from '@material-ui/core'

import RegisterIcon from '@material-ui/icons/PersonAdd'
import LogoutIcon from '@material-ui/icons/PowerSettingsNew'

import { ApplicationState } from '../../store'
import { AuthState } from '../../store/auth/types'
import { NavButton } from '../buttons'
import { LoginDialog, RegisterDialog } from '../dialogs'
import { LogOut } from '../../store/auth/action'
import styles from './style'

import LoginIcon from '@material-ui/icons/ExitToApp'


interface Props extends AuthState { 
    LogOut: Function
}

interface State {
    loginDialogOpen: boolean,
    registerDialogOpen: boolean
}

class TopBar extends Component<Props & WithStyles<typeof styles>, State> {
    state = {
        loginDialogOpen: false,
        registerDialogOpen: false
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    handleLoginDialogOpen = () => {
        this.setState({
            loginDialogOpen: true
        })
    }

    handleLoginDialogClose = () => {
        this.setState({
            loginDialogOpen: false
        })
    }

    handleRegisterDialogOpen = () => {
        this.setState({
            registerDialogOpen: true
        })
    }

    handleRegisterDialogClose = () => {
        this.setState({
            registerDialogOpen: false
        })
    }

    render() {
        const { classes, isLoggedIn, LogOut, username } = this.props
        return (
            <div className={classes.root}>
            <Grid container item>
                <AppBar position="sticky" color="default" className={classes.root}>
                    <Toolbar>
                        <Typography 
                            className={classes.title} 
                            variant="h6" 
                            color="primary" 
                            noWrap
                        >
                            <Link to='/' className={classes.styledLink}>Expense Tracker</Link>
                        </Typography>
                        <div className={classes.grow} />
                        <div>
                            {
                                isLoggedIn ?
                                    <Fragment>
                                        <Typography className={classes.username} inline color="primary" variant="button">Hi, &nbsp; {username} &nbsp; &nbsp;</Typography>
                                        <NavButton 
                                            text="Log Out" 
                                            Icon={LogoutIcon} 
                                            onClick={()=>{
                                                LogOut()
                                                localStorage.removeItem('token')
                                            }} 
                                        />
                                    </Fragment>
                                :
                                    <Fragment>
                                        <div className={classes.navButton}>
                                            <NavButton text="Log In" Icon={LoginIcon} onClick={this.handleLoginDialogOpen}/>
                                            <LoginDialog 
                                                open={this.state.loginDialogOpen}
                                                handleClose={this.handleLoginDialogClose}
                                            />
                                        </div>
                                        <div className={classes.navButton}>
                                        <NavButton text="Register" Icon={RegisterIcon} onClick={this.handleRegisterDialogOpen}/>
                                        <RegisterDialog
                                            open={this.state.registerDialogOpen}
                                            handleClose={this.handleRegisterDialogClose}
                                         />
                                        </div>
                                    </Fragment>
                            }
                        </div>
                    </Toolbar>
                </AppBar>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state: ApplicationState): AuthState => {
    return state.auth
}

const mapDispatchToProps = {
    LogOut
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TopBar))
