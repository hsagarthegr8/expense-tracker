import { createStyles, Theme } from '@material-ui/core'

const styles = (theme: Theme) =>
    createStyles({
        root: {
            position: 'fixed',
            zIndex:10,
            boxShadow: 'none',
            borderBottom: `1px solid ${theme.palette.grey['100']}`,
            backgroundColor: 'white',
        },
        grow: {
            flexGrow: 1,
        },
        title: {
            display: 'block',
        },
        username: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'inline'
            }
        },

        navButton: {
            display: 'inline-block'
        },
        styledLink: {
            textDecoration: 'none',
            color: 'inherit',
        }
    })

export default styles