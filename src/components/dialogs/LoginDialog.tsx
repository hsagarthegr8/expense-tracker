import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withFormik, FormikProps, FormikBag } from 'formik'
import * as Yup from 'yup'
import { AxiosResponse, AxiosError } from 'axios'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    DialogContentText,
} from '@material-ui/core'

import { Api, loginUrl } from '../../api'
import { logIn } from '../../store/auth/action'
import { requiredErrorMessage } from '../../utils';


interface Values extends NonFieldErrors { 
    username: string,
    password: string
}

interface NonFieldErrors {
    non_field_errors?: string
}

interface OwnProps {
    handleClose: Function
    open: boolean
}

interface Action {
    logIn: Function
}


const formContainer = withFormik<any, Values>({
    mapPropsToValues: (): Values => ({
        username: '',
        password: '',
    }),

    validationSchema: Yup.object().shape({
        username: Yup.string().required(requiredErrorMessage),
        password: Yup.string().required(requiredErrorMessage)
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<OwnProps & Action , Values>) => {
        const api = Api()
        const { logIn, handleClose } = formikBag.props
        api.post(loginUrl, values)
        .then((res: AxiosResponse) => {
            const { token } = res.data
            logIn(token)
            localStorage.setItem('token', token)
            handleClose()
        })
        .catch((err: AxiosError) => {
            if (err.response)
                formikBag.setFieldValue('non_field_errors', err.response.data.non_field_errors[0])
        })
    },
})


class LoginDialog extends Component<OwnProps & Action & FormikProps<Values>> {

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    }

    handleClose = () => {
        const { handleClose, resetForm } = this.props
        resetForm()
        handleClose()
    }

    render() {
        const { errors, touched, values, handleSubmit } = this.props
        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                fullWidth
            >
                <DialogTitle id="form-dialog-title">Login</DialogTitle>
                <DialogContent>
                    <DialogContentText color="error">{values.non_field_errors}</DialogContentText>
                    <form>
                        <br />
                        <TextField
                            name="username"
                            variant="outlined"
                            value={values.username}
                            label="Username"
                            helperText={touched.username ? errors.username : ""}
                            error={(touched.username && Boolean(errors.username)) || Boolean(values.non_field_errors)}
                            fullWidth
                            required
                            onChange={this.handleChange}
                        />
                        <br />
                        <br />
                        <TextField
                            name="password"
                            label="Passwod"
                            variant="outlined"
                            helperText={touched.password ? errors.password : ""}
                            error={(touched.password && Boolean(errors.password)) || Boolean(values.non_field_errors)}
                            value={values.password}
                            fullWidth
                            required
                            type="password"
                            onChange={this.handleChange}
                        />
                        <br />
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={()=>handleSubmit()} color="primary">
                        Login
                        </Button>
                </DialogActions>
            </Dialog>
        )
    }
}


const mapDispatchToProps = {
    logIn,
}

export default connect(null, mapDispatchToProps)(formContainer(LoginDialog))