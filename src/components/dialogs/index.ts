import LoginDialog from './LoginDialog'
import RegisterDialog from './RegisterDialog'
import WalletDialog from './WalletDialog'
import AddWalletDialog from './AddWalletDialog'
import EditWalletDialog from './EditWalletDialog'
import WarningDialog from './DeleteWalletDialog'
import TransferBalanceDialog from './TransferBalanceDialog'

export {
    LoginDialog,
    RegisterDialog,
    WalletDialog,
    AddWalletDialog,
    EditWalletDialog,
    WarningDialog,
    TransferBalanceDialog
}