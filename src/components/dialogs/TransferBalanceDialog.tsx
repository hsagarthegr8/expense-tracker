import React, { Component } from 'react'
import * as Yup from 'yup'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    MenuItem
} from '@material-ui/core'

import { connect } from 'react-redux'
import { updateWallet, transferBalanceBetweenWallet } from '../../store/wallet/action'

import { Wallet } from '../../store/wallet/types'
import { withFormik, FormikProps, FormikBag } from 'formik'
import { SelectCategory, requiredErrorMessage, getWalletsSelect } from '../../utils'

import { Api, updateWalletUrl, transferBalanceUrl} from '../../api'
import { AxiosResponse, AxiosError } from 'axios'
import { fetchTransactions } from '../../store/transactions/action';

interface OwnProps {
    open: boolean,
    wallet: Wallet,
    handleClose: Function
}

interface Values { 
    from: string,
    to?: number,
    balance?: number
}

interface Payload {
    from: number,
    to: number,
    balance: number
}

interface Action {
    transferBalanceBetweenWallet: Function,
    fetchTransactions: Function
}




const formContainer = withFormik<OwnProps & Action, Values>({
    mapPropsToValues: ({wallet}): Values => ({
        from: wallet.name,
        to: undefined,
        balance: undefined
    }),
    validationSchema: (props: OwnProps) => (
            Yup.object().shape<Values>({
            from: Yup.string().required(requiredErrorMessage),
            to: Yup.number().required(requiredErrorMessage),
            balance: Yup.number().min(0).max(props.wallet.balance, "Transfer Balance should not exceed the current balance in the wallet")
                .required(requiredErrorMessage)
            }
        )
    ),

    handleSubmit: (values: Values, formikBag: FormikBag<OwnProps & Action, Values>) => {
        const api = Api()
        const { wallet, transferBalanceBetweenWallet, fetchTransactions, handleClose } = formikBag.props
        
        if (values.to && values.balance) {
            const payload: Payload = {
                from: wallet.id,
                to: values.to,
                balance: values.balance
            }
            
            api.patch(transferBalanceUrl, payload)
            .then((res: AxiosResponse) => {
                transferBalanceBetweenWallet(wallet.id, values.to, values.balance)
                formikBag.resetForm()
                handleClose()
                fetchTransactions()
            })
            .catch((err: AxiosError) => {
                console.log(err.response)
            })
        }
    }
})



class TransferBalanceDialog extends Component<OwnProps & Action & FormikProps<Values>> {

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    } 

    render() {
        const { values, touched, errors } = this.props
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => this.props.handleClose()}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">
                        Transfer Balance
                    </DialogTitle>
                    <DialogContent>
                        <form>
                            <br />
                            <TextField
                                name="from"
                                variant="outlined"
                                value={values.from}
                                label="From"
                                fullWidth
                                required
                                disabled
                                onChange={this.handleChange}
                                helperText={touched.from ? errors.from : ""}
                                error={touched.from && Boolean(errors.from)}
                            />
                            <br />
                            <br />
                            <TextField
                                name="to"
                                variant="outlined"
                                label="To"
                                select
                                value={values.to}
                                fullWidth
                                required
                                onChange={this.handleChange}
                                helperText={touched.to ? errors.to : ""}
                                error={touched.to && Boolean(errors.to)}
                            >
                            {getWalletsSelect().filter((category: SelectCategory) => category.label != values.from)
                                .map((category: SelectCategory) => 
                                <MenuItem key={category.value} value={category.value}>
                                    {category.label}
                                </MenuItem>    
                            )}
                            </TextField>
                            <br />
                            <br /> 
                            <TextField
                                name="balance"
                                variant="outlined"
                                label="Balance"
                                value={values.balance}
                                onChange={this.handleChange}
                                fullWidth
                                required
                                type="number"
                                helperText={touched.balance ? errors.balance : ""}
                                error={touched.balance && Boolean(errors.balance)}
                            />
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>this.props.handleClose()} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={()=>this.props.handleSubmit()} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

const mapDispatchToProps = {
    transferBalanceBetweenWallet,
    fetchTransactions
}

export default connect(null, mapDispatchToProps)(formContainer(TransferBalanceDialog))
