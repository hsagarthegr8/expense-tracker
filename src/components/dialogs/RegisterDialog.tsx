import React, { Component } from 'react'
import { withFormik, FormikProps, FormikErrors, FormikBag } from 'formik'
import * as Yup from 'yup'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
} from '@material-ui/core'

import { requiredErrorMessage } from '../../utils';
import { Api, registerUrl } from '../../api';
import { AxiosResponse, AxiosError } from 'axios';


interface OwnProps{
    open: boolean,
    handleClose: Function
}
interface Values { 
    username: string,
    email: string,
    password: string,
    confirmPassword: string,
}


const formContainer = withFormik<any, Values>({
    mapPropsToValues: (): Values => ({
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
    }),

    validationSchema: Yup.object().shape({
        username: Yup.string()
            .min(8, "Username must contain atleast 8 characters")
            .max(16, "Username cannot contain more than 16 characters")
            .required(requiredErrorMessage),

        email: Yup.string()
            .email("Enter a valid email")
            .required(requiredErrorMessage),

        password: Yup.string()
            .required(requiredErrorMessage)
            .min(8, "Password must contain atleast 8 characters")
            .max(16, "Password cannot contain more than 16 characters"),

        confirmPassword: Yup.string()
            .required(requiredErrorMessage)
            .oneOf([Yup.ref('password')], "Password does not match")
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<OwnProps, Values>) => {
        const api = Api()
        api.post(registerUrl, values)
        .then((res: AxiosResponse) => {
            formikBag.resetForm()
            formikBag.props.handleClose()
        })
        .catch((err: AxiosError) => {
            const newErrors:any = {}
            if (err.response) {
                const errors = Object.entries(err.response.data)
                errors.forEach((error: any) => {
                    newErrors[error[0]] = error[1][0]
                })
                formikBag.setErrors(newErrors)
            }
        })
    }
})


class RegisterDialog extends Component<OwnProps & FormikProps<Values>> {
    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    }

    handleClose = () => {
        const { handleClose, resetForm } = this.props
        resetForm()
        handleClose()
    }

    render() {
        const { errors, handleSubmit, touched, values, open } = this.props
        return (
            <div>
                <Dialog
                    open={open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">New User?? Register Here</DialogTitle>
                    <DialogContent>
                        <form>
                            <br/>
                            <TextField
                                name="username"
                                variant="outlined"
                                value={values.username}
                                label="Username"
                                helperText={touched.username ? errors.username : ""}
                                error={touched.username && Boolean(errors.username)}
                                fullWidth
                                required
                                onChange={this.handleChange}
                            />
                            <br/>
                            <br/>
                            <TextField
                                name="email"
                                variant="outlined"
                                value={values.email}
                                label="Email"
                                helperText={touched.email ? errors.email : ""}
                                error={touched.email && Boolean(errors.email)}
                                fullWidth
                                required
                                onChange={this.handleChange}
                            />
                            <br />
                            <br />
                            <TextField
                                name="password"
                                variant="outlined"
                                label="Password"
                                helperText={touched.password ? errors.password : ""}
                                error={touched.password && Boolean(errors.password)}
                                value={values.password}
                                fullWidth
                                required
                                type="password"
                                onChange={this.handleChange}
                            />
                            <br />
                            <br />
                            <TextField
                                name="confirmPassword"
                                variant="outlined"
                                label="Confirm Password"
                                helperText={touched.confirmPassword ? errors.confirmPassword : ""}
                                error={touched.confirmPassword && Boolean(errors.confirmPassword)}
                                value={values.confirmPassword}
                                fullWidth
                                required
                                type="password"
                                onChange={this.handleChange}
                            />
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={()=>handleSubmit()} color="primary">
                            Register Now
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default formContainer(RegisterDialog)