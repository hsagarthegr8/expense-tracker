import React, { Component } from 'react'
import * as Yup from 'yup'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    MenuItem
} from '@material-ui/core'

import { connect } from 'react-redux'
import { updateWallet } from '../../store/wallet/action'

import { Wallet } from '../../store/wallet/types'
import { withFormik, FormikProps, FormikBag } from 'formik'
import { walletCategories, SelectCategory, requiredErrorMessage } from '../../utils'

import { Api, updateWalletUrl} from '../../api'
import { AxiosResponse, AxiosError } from 'axios'

interface OwnProps {
    open: boolean,
    wallet: Wallet,
    handleClose: Function
}

interface Values extends Partial<Wallet> { }

interface Action {
    updateWallet: Function
}



const formContainer = withFormik<OwnProps & Action, Values>({
    mapPropsToValues: ({wallet}): Values => ({
        name: wallet.name,
        category: wallet.category,
        balance: wallet.balance
    }),

    validationSchema: Yup.object().shape({
        name: Yup.string().required(requiredErrorMessage),
        category: Yup.string().required(requiredErrorMessage),
        balance: Yup.number().required(requiredErrorMessage)
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<OwnProps & Action, Values>) => {
        const api = Api()
        const { wallet, updateWallet, handleClose } = formikBag.props
        api.put(updateWalletUrl(wallet.id), values)
        .then((res: AxiosResponse) => {
            updateWallet(res.data)
            formikBag.resetForm()
            handleClose()
        })
        .catch((err: AxiosError) => {
            console.log(err.response)
        })
        
    }
})



class EditWalletDialog extends Component<OwnProps & Action & FormikProps<Values>> {

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    } 

    render() {
        const { values, touched, errors } = this.props
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => this.props.handleClose()}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">
                        Edit Wallet
                    </DialogTitle>
                    <DialogContent>
                        <form>
                            <br />
                            <TextField
                                name="name"
                                variant="outlined"
                                value={values.name}
                                label="Name"
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.name ? errors.name : ""}
                                error={touched.name && Boolean(errors.name)}
                            />
                            <br />
                            <br />
                            <TextField
                                name="category"
                                variant="outlined"
                                label="Category"
                                select
                                value={values.category}
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.category ? errors.category : ""}
                                error={touched.category && Boolean(errors.category)}
                            >
                            {walletCategories.map((category: SelectCategory) => 
                                <MenuItem key={category.value} value={category.value}>
                                    {category.label}
                                </MenuItem>    
                            )}
                            </TextField>
                            <br />
                            <br />
                            <TextField
                                name="balance"
                                variant="outlined"
                                label="Balance"
                                value={values.balance}
                                onChange={this.handleChange}
                                fullWidth
                                type="number"
                                helperText={touched.balance ? errors.balance : ""}
                                error={touched.balance && Boolean(errors.balance)}
                            />
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>this.props.handleClose()} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={()=>this.props.handleSubmit()} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

const mapDispatchToProps = {
    updateWallet
}

export default connect(null, mapDispatchToProps)(formContainer(EditWalletDialog))
