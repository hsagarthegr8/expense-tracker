import React, { Component } from 'react'
import * as Yup from 'yup'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    MenuItem
} from '@material-ui/core'
import { DatePicker } from 'material-ui-pickers'
import moment from 'moment'

import { connect } from 'react-redux'
import { addTransaction } from '../../store/transactions/action'

import { withFormik, FormikProps, FormikBag } from 'formik'
import { SelectCategory, transactionTypes, getWalletsSelect } from '../../utils'

import { addTransactionUrl, Api} from '../../api'
import { AxiosResponse, AxiosError } from 'axios'
import { Transaction } from '../../store/transactions/types';
import { fetchWallets } from '../../store/wallet/action';

interface OwnProps {
    open: boolean,
    handleClose: Function
}


interface Values extends Partial<Transaction> { }

interface Action {
    addTransaction: Function
    fetchWallets: Function
}



const formContainer = withFormik<OwnProps & Action, Values>({
    mapPropsToValues: (): Values => ({
        description: '',
        type: '',
        date: new Date(),
        wallet: undefined,
        amount: undefined
    }),

    validationSchema: Yup.object().shape({
        description: Yup.string().required(),
        type: Yup.string().required(),
        date: Yup.date().required(),
        wallet: Yup.number().required(),
        amount: Yup.number().required()
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<OwnProps & Action, Values>) => {
        const api = Api()
        const { handleClose, addTransaction, fetchWallets } = formikBag.props
        api.post(addTransactionUrl, {
            ...values,
            date: moment(values.date).format('YYYY-MM-DD')
        })
        .then((res: AxiosResponse) => {
            formikBag.resetForm()
            handleClose()
            addTransaction(res.data)
            fetchWallets()
        })
        .catch((err: AxiosError) => {

        })
    }
})



class AddTransactionDialog extends Component<OwnProps & Action & FormikProps<Values>> {

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    } 

    handleDateChange = (value:Date) => {
        const {
            setFieldValue,
            setFieldTouched
        } = this.props
        setFieldValue("date", value, true)
        setFieldTouched("date")
    }

    handleClose = () => {
        this.props.resetForm()
        this.props.handleClose()
    }

    render() {
        const { values, touched, errors } = this.props
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => this.handleClose()}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">
                        Add Transaction
                    </DialogTitle>
                    <DialogContent>
                        <form>
                            <br />
                            <TextField
                                required
                                name="description"
                                variant="outlined"
                                value={values.description}
                                label="Description"
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.description ? errors.description : ""}
                                error={touched.description && Boolean(errors.description)}
                            />
                            <br />
                            <br />
                            <TextField
                                required
                                name="type"
                                variant="outlined"
                                label="Type"
                                select
                                value={values.type}
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.type ? errors.type : ""}
                                error={touched.type && Boolean(errors.type)}
                            >
                            {transactionTypes.map((category: SelectCategory) => 
                                <MenuItem key={category.value} value={category.value}>
                                    {category.label}
                                </MenuItem>    
                            )}
                            </TextField>
                            <br />
                            <br />
                            <TextField
                                required
                                name="amount"
                                variant="outlined"
                                label="Amount"
                                value={values.amount}
                                onChange={this.handleChange}
                                fullWidth
                                type="number"
                                helperText={touched.amount ? errors.amount : ""}
                                error={touched.amount && Boolean(errors.amount)}
                            />
                            <br/>
                            <br/>
                            <TextField
                                required
                                name="wallet"
                                variant="outlined"
                                label="Wallet"
                                select
                                value={values.wallet || ''}
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.wallet ? errors.wallet : ""}
                                error={touched.wallet && Boolean(errors.wallet)}
                            >
                            {getWalletsSelect().map((category: SelectCategory) => 
                                <MenuItem key={category.value} value={category.value}>
                                    {category.label}
                                </MenuItem>    
                            )}
                            </TextField>
                            <br/>
                            <br/>
                            <DatePicker
                                required
                                keyboard
                                fullWidth
                                name="date"
                                variant="outlined"
                                label="Date"
                                value={values.date}
                                onChange={this.handleDateChange}
                            />
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.handleClose()} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={()=>this.props.handleSubmit()} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

const mapDispatchToProps = {
    addTransaction,
    fetchWallets
}

export default connect(null, mapDispatchToProps)(formContainer(AddTransactionDialog))