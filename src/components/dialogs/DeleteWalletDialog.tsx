import React, { FC, Component } from 'react'
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import { Wallet } from '../../store/wallet/types';
import { Api, deleteWalletUrl } from '../../api';
import { AxiosResponse, AxiosError } from 'axios';
import { connect } from 'react-redux'
import { deleteWallet } from '../../store/wallet/action';
import { deleteAllTransactionsOfAWallet } from '../../store/transactions/action';

interface Props {
    open: boolean,
    wallet: Wallet,
    handleClose: Function,
}

interface Action {
    deleteWallet: Function
    deleteAllTransactionsOfAWallet: Function
}

class DeleteWalletDialog extends Component<Props & Action> {
    handleSubmit = () => {
        const { wallet, handleClose, deleteWallet, deleteAllTransactionsOfAWallet } = this.props
        const api = Api()
        api.delete(deleteWalletUrl(wallet.id))
        .then((res:AxiosResponse) => {
            deleteWallet(wallet)
            deleteAllTransactionsOfAWallet(wallet)
            handleClose()
        })
        .catch((err: AxiosError) => handleClose())
    }

    render() {
        const { handleClose, open } = this.props
        return (
            <Dialog
                open={open}
                onClose={()=>handleClose()}
                fullWidth
            >
                <DialogTitle>Delete Wallet</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Are you sure you want to delete the wallet and its associated transactions.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=>handleClose()} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSubmit} color="primary">
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

const mapDispatchToProps: Action = {
    deleteWallet,
    deleteAllTransactionsOfAWallet
}

export default connect(null, mapDispatchToProps)(DeleteWalletDialog)