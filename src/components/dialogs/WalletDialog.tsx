import React, { Component } from 'react'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    MenuItem
} from '@material-ui/core'
import { withFormik, FormikProps } from 'formik'
import * as Yup from 'yup'

import { Wallet } from '../../store/wallet/types'

import { walletCategories, SelectCategory } from '../../utils'

interface Props {
    open: boolean,
    editMode: boolean,
    wallet?: Wallet,
    onSubmit?: Function
    handleClose: Function
}

interface State {
    editMode: boolean
}

interface Values extends Partial<Wallet> { }



const formContainer = withFormik<Props, Values>({
    mapPropsToValues: (props: Props): Values => {
        const { wallet } = props
        if (wallet) {
            return {
                name: wallet.name,
                category: wallet.category,
                balance: wallet.balance
            }
        }
        return {
            name:'',
            category:'',
            balance:0
        }

    },

    validationSchema: Yup.object().shape({
        name: Yup.string().required(),
        category: Yup.string().required(),
        balance: Yup.number().required()
    }),

    handleSubmit: values => {
        console.log(values)
    }
})


class WalletDialog extends Component<Props & FormikProps<Values>> {

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    } 

    render() {
        const { values, touched, errors, editMode } = this.props
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => this.props.handleClose()}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">
                        {editMode ? "Edit Wallet": "Add Wallet"}
                    </DialogTitle>
                    <DialogContent>
                        <form>
                            <br />
                            <TextField
                                name="name"
                                variant="outlined"
                                value={values.name}
                                label="Name"
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.name ? errors.name : ""}
                                error={touched.name && Boolean(errors.name)}
                            />
                            <br />
                            <br />
                            <TextField
                                name="category"
                                variant="outlined"
                                label="Category"
                                select
                                value={values.category}
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.category ? errors.category : ""}
                                error={touched.category && Boolean(errors.category)}
                            >
                            {walletCategories.map((category: SelectCategory) => 
                                <MenuItem key={category.value} value={category.value}>
                                    {category.label}
                                </MenuItem>    
                            )}
                            </TextField>
                            <br />
                            <br />
                            <TextField
                                name="balance"
                                variant="outlined"
                                label="Balance"
                                value={values.balance}
                                onChange={this.handleChange}
                                fullWidth
                                type="number"
                                helperText={touched.balance ? errors.balance : ""}
                                error={touched.balance && Boolean(errors.balance)}
                            />
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.props.handleClose()} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={() => this.props.onSubmit} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}


export default formContainer(WalletDialog)