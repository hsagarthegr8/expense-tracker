import React, { Component } from 'react'
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import { Api, deleteTransactionUrl } from '../../api';
import { AxiosResponse, AxiosError } from 'axios';
import { connect } from 'react-redux'
import { Transaction } from '../../store/transactions/types';
import { deleteTransaction } from '../../store/transactions/action';
import { fetchWallets } from '../../store/wallet/action';


interface Props {
    open: boolean,
    transaction: Transaction,
    handleClose: Function,
}

interface Action {
    deleteTransaction: Function
    fetchWallets: Function
}

class DeleteWalletDialog extends Component<Props & Action> {
    handleSubmit = () => {
        const { transaction, handleClose, deleteTransaction, fetchWallets } = this.props
        const api = Api()
        api.delete(deleteTransactionUrl(transaction.id))
        .then((res:AxiosResponse) => {
            handleClose()
            deleteTransaction(transaction)
            fetchWallets()
        })
        .catch((err: AxiosError) => handleClose())
    }

    render() {
        const { handleClose, open } = this.props
        return (
            <Dialog
                open={open}
                onClose={()=>handleClose()}
                fullWidth
            >
                <DialogTitle>Delete Transaction</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Are you sure you want to delete the transaction ?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=>handleClose()} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSubmit} color="primary">
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

const mapDispatchToProps: Action = {
    deleteTransaction,
    fetchWallets
}

export default connect(null, mapDispatchToProps)(DeleteWalletDialog)