import React, { Component } from 'react'
import * as Yup from 'yup'
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    MenuItem
} from '@material-ui/core'

import { connect } from 'react-redux'
import { addWallet } from '../../store/wallet/action'

import { Wallet } from '../../store/wallet/types'
import { withFormik, FormikProps, FormikBag } from 'formik'
import { walletCategories, SelectCategory, requiredErrorMessage } from '../../utils'

import { addWalletUrl, Api} from '../../api'
import { AxiosResponse, AxiosError } from 'axios'

interface OwnProps {
    open: boolean,
    handleClose: Function
}


interface Values extends Partial<Wallet> { }

interface Action {
    addWallet: Function
}



const formContainer = withFormik<OwnProps & Action, Values>({
    mapPropsToValues: (): Values => ({
        name:'',
        category:'',
        balance: undefined
    }),

    validationSchema: Yup.object().shape({
        name: Yup.string().required(requiredErrorMessage),
        category: Yup.string().required(requiredErrorMessage),
        balance: Yup.number().required(requiredErrorMessage)
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<OwnProps & Action, Values>) => {
        const api = Api()
        const { addWallet, handleClose } = formikBag.props
        api.post(addWalletUrl, values)
        .then((res: AxiosResponse) => {
            addWallet(res.data)
            formikBag.resetForm()
            handleClose()
        })
        .catch((err: AxiosError) => {
            console.log(err.response)
        })
        
    }
})



class AddWalletDialog extends Component<OwnProps & Action & FormikProps<Values>> {

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    }

    handleClose = () => {
        const { resetForm, handleClose } = this.props
        resetForm()
        handleClose()
    }

    render() {
        const { values, touched, errors } = this.props
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">
                        Add Wallet
                    </DialogTitle>
                    <DialogContent>
                        <form>
                            <br />
                            <TextField
                                required
                                name="name"
                                variant="outlined"
                                value={values.name}
                                label="Name"
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.name ? errors.name : ""}
                                error={touched.name && Boolean(errors.name)}
                            />
                            <br />
                            <br />
                            <TextField
                                required
                                name="category"
                                variant="outlined"
                                label="Category"
                                select
                                value={values.category}
                                fullWidth
                                onChange={this.handleChange}
                                helperText={touched.category ? errors.category : ""}
                                error={touched.category && Boolean(errors.category)}
                            >
                            {walletCategories.map((category: SelectCategory) => 
                                <MenuItem key={category.value} value={category.value}>
                                    {category.label}
                                </MenuItem>    
                            )}
                            </TextField>
                            <br />
                            <br />
                            <TextField
                                required
                                name="balance"
                                variant="outlined"
                                label="Balance"
                                value={values.balance}
                                onChange={this.handleChange}
                                fullWidth
                                type="number"
                                helperText={touched.balance ? errors.balance : ""}
                                error={touched.balance && Boolean(errors.balance)}
                            />
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={()=>this.props.handleSubmit()} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

const mapDispatchToProps = {
    addWallet
}

export default connect(null, mapDispatchToProps)(formContainer(AddWalletDialog))