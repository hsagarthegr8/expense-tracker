import React, { Component } from 'react'
import { Paper, Button, Typography, Grid, WithStyles, createStyles, Theme, withStyles } from '@material-ui/core'
import WalletDashboard from './WalletDashboard'

import { ApplicationState } from '../store'
import { connect } from 'react-redux'
import TransactionDashboard from './TransactionDashboard'


const backgroundShape = require('../images/shape.svg')

const styles = (theme: Theme) => createStyles({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.grey['100'],
        overflow: 'hidden',
        background: `url(${backgroundShape}) no-repeat`,
        backgroundSize: 'cover',
        backgroundPosition: '0 400px',
        paddingBottom: 200
    },
    grid: {
        width: 1200,
        marginTop: 30,
        [theme.breakpoints.down('sm')]: {
            width: 'calc(100% - 20px)'
        }
    },
})

interface Props {
    isLoggedIn: boolean
}

class Dashboard extends Component<Props & WithStyles<typeof styles>> {
    render() {
        const { classes, isLoggedIn } = this.props
        return (
            <div className={classes.root}>
                <Grid container justify="center">
                    <Grid 
                        spacing={24} 
                        alignItems="center" 
                        justify="center" 
                        container 
                        className={classes.grid}
                    >
                        <Grid container item xs={12}>
                            <Grid item xs={12}>
                                {isLoggedIn ? 
                                    <WalletDashboard />
                                    : null
                                }
                            </Grid>
                        </Grid>
                        <Grid container item xs={12}>
                            <Grid item xs={12}>
                                {isLoggedIn ? 
                                    <TransactionDashboard />
                                    : null
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state: ApplicationState): Props => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    }
}

export default connect(mapStateToProps)(withStyles(styles)(Dashboard))