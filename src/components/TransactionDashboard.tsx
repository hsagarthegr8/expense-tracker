import React, { Component,  } from 'react'
import { connect } from 'react-redux'
import { 
    Tooltip,
    Fab,
    withStyles, 
    Theme, 
    WithStyles, 
    createStyles, 
    Paper, 
    Typography, 
    List 
} from '@material-ui/core'

import AddIcon from '@material-ui/icons/Add'

import { ApplicationState } from '../store'
import { Transaction } from '../store/transactions/types'
import { fetchTransactions } from '../store/transactions/action'
import TransactionListItem from './TransactionListItem'
import AddTransactionDialog from './dialogs/AddTransactionDialog';
import DeleteTransactionDialog from './dialogs/DeleteTransactionDialog';
import EditTransacionDialog from './dialogs/EditTransacionDialog';

const styles = (theme: Theme) => createStyles({
    paper: {
        padding: theme.spacing.unit * 3,
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    box: {
        marginBottom: 40,
    },
    alignRight: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    actionButtom: {
        textTransform: 'uppercase',
        margin: theme.spacing.unit,
        width: 152
    },
})

interface Props {
    transactions : Transaction[]
}

interface DialogState { 
    add: boolean
    edit: boolean
    delete: boolean
}
interface State {
    dialog: DialogState
    selectedTransaction?: Transaction
}

interface Action {
    fetchTransactions: Function
}

class TransactionDashboard extends Component<Props & Action & WithStyles<typeof styles>, State> {
    state = {
        dialog: {
            add: false,
            edit: false,
            delete: false
        },
        selectedTransaction: undefined
    }

    componentDidMount() {
        const { fetchTransactions } = this.props
        fetchTransactions()
    }

    handleDialogOpen = (dialog: string) => {
        this.setState((prevState: State) => ({
            dialog: {
                ...prevState.dialog,
                [dialog]: true
            }
        }))
    }

    handleDialogClose = (dialog: string) => {
        this.setState((prevState: State) => ({
            dialog: {
                ...prevState.dialog,
                [dialog]: false
            }
        }))
    }

    handleSelect = (transaction: Transaction, dialog: string) => {
        this.setState({
            selectedTransaction: transaction
        })
        this.handleDialogOpen(dialog)
    }

    render() {
        const { classes, transactions } = this.props
        const { dialog, selectedTransaction} = this.state
        return (
            <Paper className={classes.paper}>
                <div className={classes.box}>
                    <Typography variant="h6">
                        Your Transactions
                        <Tooltip title="Add Transaction">
                            <Fab 
                                size="small"
                                style={{float: "right"}} 
                                color="primary" 
                                onClick={()=>this.handleDialogOpen('add')}
                            >
                                <AddIcon />
                            </Fab>
                        </Tooltip>
                        <AddTransactionDialog 
                            open={dialog.add} 
                            handleClose={()=>this.handleDialogClose('add')}
                        />
                    </Typography>
                    {transactions.length ?
                        <List>
                            { transactions.map((transaction: Transaction) => (
                                <TransactionListItem 
                                    key={transaction.id} 
                                    transaction={transaction} 
                                    handleSelect={this.handleSelect}
                                />
                            )) 
                            }
                        </List>
                    :
                    null
                    }
                    { dialog.edit && selectedTransaction ?
                        <EditTransacionDialog 
                            transaction={selectedTransaction} 
                            open={dialog.edit}
                            handleClose={()=>this.handleDialogClose('edit')} 
                        />
                    :
                    null
                    }
                    { dialog.delete && selectedTransaction ?
                        <DeleteTransactionDialog 
                            transaction={selectedTransaction}
                            open={dialog.delete}
                            handleClose={()=>this.handleDialogClose('delete')}
                        />
                        :
                        null
                    }
                </div>
            </Paper>
        )
    }
}

const mapStateToProps = (state: ApplicationState): Props => {
    return {
        transactions: state.transactions
    }
}

const mapDispatchToProps = {
    fetchTransactions
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TransactionDashboard))