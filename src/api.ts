import axios, { AxiosRequestConfig } from 'axios'
import store from './store'

export const Api = () => {
    const state = store.getState().auth
    if (state.isLoggedIn) {
        return axios.create({
            baseURL: 'http://127.0.0.1:8000/api',
            headers: {
                Authorization: `JWT ${state.token}`
            }
        })
    }
    return axios.create({
        baseURL: 'http://127.0.0.1:8000/api'
    })
}

// export const Api = () => {
//     const state = store.getState().auth
//     if (state.isLoggedIn) {
//         return axios.create({
//             baseURL: 'https://warm-reef-92847.herokuapp.com/api',
//             headers: {
//                 Authorization: `JWT ${state.token}`
//             }
//         })
//     }
//     return axios.create({
//         baseURL: 'https://warm-reef-92847.herokuapp.com/api'
//     })
// }




export const loginUrl = 'get-token/'
export const verifyTokenUrl = 'verify-token/'
export const registerUrl = 'accounts/new/'
export const addWalletUrl = 'wallets/new/'
export const deleteWalletUrl = (id: number) => `wallets/${id}/delete/`
export const updateWalletUrl = (id: number) => `wallets/${id}/update/`
export const transferBalanceUrl = 'wallets/transfer/'
export const addTransactionUrl = 'transactions/new/'
export const updateTransactionUrl = (id: number) => `transactions/${id}/update/`
export const deleteTransactionUrl = (id: number) => `transactions/${id}/delete/`