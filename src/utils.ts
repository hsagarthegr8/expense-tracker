import { Wallet } from "./store/wallet/types"
import store from "./store";

export const requiredErrorMessage = "This is a required field."

export const getCategory = (wallet: Wallet): string => {
    switch (wallet.category) {
        case 'B':
            return "Bank Account"
        case 'W':
            return "Digital Wallet"
        default:
            return "Cash Balance"
    }
}

export interface SelectCategory {
    label: string | number,
    value: string | number
}

export const walletCategories: SelectCategory[] = [
    {
        value: 'B',
        label: 'Bank Account'
    },
    {
        value: 'C',
        label: 'Cash Balance'
    },
    {
        value: 'W',
        label: 'Digital Wallet'
    }
]

export const transactionTypes: SelectCategory[] = [
    {
        value: 'I',
        label: 'Income'
    },
    {
        value: 'E',
        label: 'Expense'
    }
]

export const getWalletsSelect = ():SelectCategory[] => {
    const wallets = store.getState().wallets

    return wallets.map((wallet:Wallet) => ({
        label: wallet.name,
        value: wallet.id
    }))
}